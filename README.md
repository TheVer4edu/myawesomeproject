## Simple Git guide

- git config --global КЛЮЧ "ЗНАЧЕНИЕ" - Установить переменные для гита (user.name и user.email)
- git config --list - Вывести значения установленных переменных 
- git init - Создать репозиторий
- git add [файл(ы)] - Добавить файлы в индекс для коммита
- git commit -m "Message" - Создать коммит с меткой Message
- git log - Вывести историю коммитов на текущей ветке
- git status - Текущее состояние репозитория
- git remote - Список подключённых удалённых репозиториев
- git remote add name url - Добавить репозиторий url по имени name
- git push -u name branch - Отправить изменения на удалённый репозиторий name с ветки branch
(В основном выглядит как git push -u origin master)
- git fetch - Пронюхать изменения на удалённом репозитории, но не получать их
- git pull - Получить изменения на удалённом репозитории
- git branch NAME - создать ветку по имени NAME
- git branch - Вывести все ветки + текущую
- git checkout NAME - переключиться на другую ветку
- git merge NAME - Объеденить ветку NAME с текущей веткой (узнать текущую можно с помощью git branch (она зелёная))
